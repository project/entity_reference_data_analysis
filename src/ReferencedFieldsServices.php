<?php

namespace Drupal\entity_reference_data_analysis;

/**
 * Class ReferencedFieldsServices.
 */
class ReferencedFieldsServices {

  protected $target_type;

  /**
   * Constructs a new ReferencedFieldsServices object.
   */
  public function __construct() {
    $this->target_type = 'node';
  }

  public function getReferencedFieldDetails($target_type = '') {
    $response_data = [];
    if (empty($target_type)) {
      $fieldData = $entity_manager = \Drupal::entityManager()->getStorage('field_storage_config')->loadByProperties(array('type' => 'entity_reference'));
    } else {
      $fieldData = $entity_manager = \Drupal::entityManager()->getStorage('field_storage_config')->loadByProperties(array('type' => 'entity_reference', 'settings' => array('target_type' => $target_type)));
    }
    $response_data = $this->preprocessFieldData($fieldData, $target_type);
    return $fieldData;
  }

  public function preprocessFieldData($data = '', $target_type) {
    // if(){
    // }
    // foreach ($data as $key => $value) {
    //   dump($value->getName());
    //   dump($value);
    // }
  }
public function getReferencedFieldName() {
  
  $fieldList = array_keys($this->getReferencedFieldDetails());
  $fieldData = [];
  foreach ($fieldList as $field) {
//    $fieldData[$field]['bundle'] = explode('.', $field)[0];
    $fieldData[$field]['bundle'] = explode([])[0];
    $fieldData[$field]['fieldName'] = explode('.', $field)[1];
  }
  
  return $fieldData;
}

  public function getFieldData($fieldName = NULL, $entityType = NULL) {
    $storageTable = $entityType . '__' . $fieldName;
    $fields = ['bundle', $fieldName . '_target_id'];
    $data = [];
    $database = \Drupal::database();
    $query = $database->select($storageTable, 'f');
    $query->fields('f', $fields);

    try {
      if ($result = $query->execute()) {
        $data = $result->fetchAll();
      }
    } catch (\Exception $e) {
      \Drupal::logger('entity_reference_data_analysis')->notice($e->getMessage());
    }
    return $data;
  }
}
